/*	Usage: RemoveNoise <input_videofile>
 * 	Author: Frederik S. Leira
 * 	Description:
 *	Program that reads images from a video file, 10 frames at a time,
 *	removing any constant noise pattern present in the 10 frames.
 */
#include <stdio.h>
#include <opencv/cv.h>
#include <vector>
#include "opencv2/opencv.hpp"
#include "opencv2/photo/photo.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace std;
using namespace cv;

int main(int argc, char** argv) {
	VideoCapture video("test.avi");
	if (!video.isOpened()) {
		cout << "Could not open video file" << endl;
		return -1;
	}

	vector<Mat> frames;
	namedWindow("video", 1);
	Mat frame;
	for(int i = 0; i<10; i++)
	{
		video >> frame;
		frames.push_back(frame);
	}

	VideoWriter noisefree("noisefree.avi", CV_FOURCC('D','I','V','X'), 30, frame.size(), true);
	int framenumber = 0;
	while(!(frames[9].empty()))
	{
		framenumber+=1;
		Mat reduced_noise;
		Mat new_frame;
		//fastNlMeansDenoisingMulti(InputArrayOfArrays srcImgs, OutputArray dst, int imgToDenoiseIndex, int temporalWindowSize, float h=3, int templateWindowSize=7, int searchWindowSize=21 )

		fastNlMeansDenoisingMulti(frames, reduced_noise, 4, 3, 3.0, 21);

		noisefree << reduced_noise;

		vector<Mat> newframes;
		for(int i = 1; i<10; i++)
		{
			newframes.push_back(frames[i]);
		}
		video >> new_frame;
		newframes.push_back(new_frame);
		frames = newframes;
		if(!(framenumber%10))
		{
			cout << "..." << endl;
		}
	}

	return 0;
}

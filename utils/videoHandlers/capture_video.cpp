/*
  record_video

  Records records video from a video link, and synchronizes each frame
  to GPS data sent from Dune (IMC messages)
  Controls:
	-c stops recording both video and GPS
  NOTE: As of 16/06/2013 the 'c' key has to be pressed for the
	synchronization to be stored.
*/
#include <stdio.h>
#include "cv.h"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <pthread.h>

#include <DUNE/DUNE.hpp>

using namespace std;
using namespace cv;
using namespace DUNE;

string window_name = "Capture";

RNG rng(12345);

double latest_timestamp;
pthread_mutex_t mutextimestamp;
bool still_capturing = true;

Network::Address* source = NULL;

void saveEstimatedStates(IMC::Message* msg, int framenumber, ofstream& outputFile)
{
	IMC::EstimatedState* estimated_state = (IMC::EstimatedState*) msg;
	if(outputFile) {
		outputFile << framenumber << ": ";// << msg->x << " " << msg->y;
		msg->toText(outputFile);
		outputFile << endl << endl;
		//msg->toText(cout);
		//cout << endl << endl;
	}
	
}

IMC::Message* getEstimatedStates(Network::UDPSocket& con, Network::Address* source)
{
	IMC::Message* msg = NULL;
	//IMC::EstimatedState* tmsg = new IMC::EstimatedState;
	int len = 65535;
	uint8_t* bfr = new uint8_t[65535];
	uint16_t rv = con.read((char*)bfr, len, source); 	
	msg = IMC::Packet::deserialize(bfr, rv);	
	//tmsg = msg;
	return msg;
}

void *captureGPS(void *threadid)
{

	ofstream outputFile;
	outputFile.open("states.txt");
	if(!outputFile) {
		printf("Failed to open save file\n");
		pthread_exit(NULL);
	}

  	IMC::Message* msg;
	Network::UDPSocket con;
	con.bind(6003, *source, true);
	int imc_message = 1;
	while( still_capturing )
    	{
	
      		msg = getEstimatedStates(con,source);
		cout << "Recieved message." << endl;
		if(msg->getSubId() == 0) {
			pthread_mutex_lock(&mutextimestamp);
			latest_timestamp = imc_message;
			pthread_mutex_unlock(&mutextimestamp);		
			saveEstimatedStates(msg, imc_message, outputFile);
			imc_message++;      			
		}	
  	}
	outputFile.close();
	cout << "GPS Receive stopped." << endl;
}

void *captureVideo(void *threadid) 
{
	//int latest_timestamp = 0;
	Mat frame;  
	ofstream outputFile;
	outputFile.open("img_vs_gps.txt");

	//source = new Network::Address(argv[0]);
	//uint16_t port = atoi(argv[1]);

  	VideoCapture capture(1);
  	if( !capture.isOpened() ) 
	{
 		 printf("Camera failed to open!\n");
        	 pthread_exit(NULL);
  	}
	//720x486

	/////720,486
  	VideoWriter record("01.avi", CV_FOURCC('D','I','V','3'), 30, Size(720,486), true);
  	namedWindow(window_name,1);
  	if( !record.isOpened() )
	{
		printf("VideoWriter failed to open!\n");
        	pthread_exit(NULL);
  	}
	int framenumber = 1;
	while( true )
    	{
      		capture >> frame;
		//cout << frame.size();
      		imshow(window_name, frame);
		pthread_mutex_lock(&mutextimestamp);
		outputFile << framenumber << ":" << latest_timestamp << ",";
		pthread_mutex_unlock(&mutextimestamp);
		record << frame;
		//cout << frame.size() << endl;
		framenumber++;
      		int c = waitKey(1000/30);
      		if( (char)c == 'c' ) { break; } 

  	}
	outputFile.close();
	still_capturing = false;
	
	cout << "Capture terminated" << endl;
}

/**
 * @function main
 */
int main( int argc, const char** argv )
{
	pthread_mutex_init(&mutextimestamp, NULL);	
	source = new Network::Address(argv[0]);	
	//cout << "TEST: " << argv[1] << endl;
	latest_timestamp = 0;
	pthread_t threads[2];

	int rc;
	
	rc = pthread_create(&threads[0], NULL, captureVideo, (void *)0);
	rc = pthread_create(&threads[1], NULL, captureGPS, (void *)1);

	while(1) { sleep(1000); }
  	return 0;
}

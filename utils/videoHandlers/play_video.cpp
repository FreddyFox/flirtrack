/*
   play_video <filename>
   
   Plays <filename> frame by frame, saving frames as .jpg on request
   Controls:
        -n 5  frames forward
	-m 10 frames forward
	-w write frame as .jpg
*/
#include <stdio.h>
#include "cv.h"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <pthread.h>


using namespace std;
using namespace cv;

void skipFrames(VideoCapture video, int framesToSkip)
{
	Mat frame;	
	for(int i = 0; i<framesToSkip; i++)
	{
		video >> frame;
		if(frame.empty()) break;
	}
}

int main(int argc, const char** argv)
{
	string window_name = "Play Video";
	int key;

	stringstream input_file;
	input_file << (string) argv[1];
	
	VideoCapture video(input_file.str().c_str());
	if(!video.isOpened()) {
		cout << "Failed to Open File: " << input_file << endl;
		return -1;
	}
	Mat frame;
	video >> frame;
	Mat history[5]; //store last 5 frames - should be a stack - use OpenCV seek function instead
	int framenumber = 1;
	while(!frame.empty()) {
		imshow(window_name, frame);
		key = waitKey(0);
		if(key == 'n')
		{
			skipFrames(video, 5);
			framenumber+=5;
		} 
		else if (key == 'm') 
		{
			skipFrames(video, 10);
			framenumber+=10;
		}
		else if (key == 'w')
		{
			stringstream filename;
			filename << "img" << framenumber << ".jpg";
			imwrite(filename.str().c_str(), frame);
		}	
		video >> frame;
		cout << "#:" << framenumber << endl;
		framenumber++;
	}
	
	return 0;
}

/* 
   edit_video <input_file> <output_file>
   
   Plays <input_file> frame by frame, letting the user mark which frames
   should be stored to <output_file>
   Controls:
	-w write frame to <output_file>
	-any other key skips to next frame
*/
#include <stdio.h>
#include "cv.h"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <pthread.h>

#include <DUNE/DUNE.hpp>

using namespace std;
using namespace cv;

int main( int argc, const char** argv )
{
	string window_name = "Capture";
	int key;
	
	stringstream input_file;
	input_file << (string) argv[1];	
	
	stringstream output_file;
	output_file << (string) argv[2];
	cout << output_file.str() << endl;
	VideoCapture capture(input_file.str().c_str());
  	if( !capture.isOpened() )  {
 		 printf("Camera failed to open!\n");
        	 return -1;
  	}

  	VideoWriter record(output_file.str().c_str(), CV_FOURCC('D','I','V','3'), 30, Size(720,486), true);

	int framenumber = 1;
	
	Mat frame;
	
  	namedWindow(window_name,1);
	while( key != 'q' )
	{

		capture >> frame;
		Mat tmpFrame = frame.clone();
		
		if(frame.empty()) break;
		stringstream text;
		int totalsec = (framenumber)*(double)(0.033333333);
		int min = totalsec/60;
		int sec = totalsec%60;
		//cout << min << " : " << sec << " -- " << totalsec << endl;
		text << "Frame#/Time: " << framenumber << "/" << (int)min << "m" << (int)sec << "s";
		putText(tmpFrame, text.str(), cvPoint(270,20), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(200,200,250), 1, CV_AA);
		imshow(window_name, tmpFrame);
		framenumber++;
		key = waitKey(0);
		if(key == 'w') {
			record << frame;
		}
	}

  	return 0;
}

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "FilterManager.h"
#include "TrackingManager.h"
#include "hungarian.h"

using namespace std;
using namespace cv;

FilterManager::FilterManager() 
{
	vector<KalmanFilter*> new_kalmanfilters;
	vector<int> new_active_kalmanfilters;
	kalmanfilters = new_kalmanfilters;
	active_kalmanfilters = new_active_kalmanfilters;
}

KalmanFilter* FilterManager::init_kf(Point measurePt)
{
	/* Sets up Kalmanfilter with 4 states,
           2 for position and 2 for velocity*/
	
	//Create kalmanfilter
	KalmanFilter* new_kf = new KalmanFilter(4,2);
	Mat_<float> state(4, 1); /* (x, y, Vx, Vy) */
	Mat processNoise(4, 1, CV_32F);
	
	//Kalmanfilter settings
	new_kf->statePre.at<float>(0) = measurePt.x;
	new_kf->statePre.at<float>(1) = measurePt.y;
	new_kf->statePre.at<float>(2) = 0;
	new_kf->statePre.at<float>(3) = 0;
	new_kf->transitionMatrix = *(Mat_<float>(4, 4) << 1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);
	Mat H = (Mat_<float>(2,4) << 1, 0, 0, 0, 0, 1, 0, 0);
	new_kf->measurementMatrix = H;
	//1e-5 Q , 1e-2 R
	setIdentity(new_kf->processNoiseCov, Scalar::all(1e-2));
	setIdentity(new_kf->measurementNoiseCov, Scalar::all(1e-1)); //Determines the importance of a measurement
	setIdentity(new_kf->errorCovPost, Scalar::all(1));
	return new_kf;
}


void FilterManager::add_kf(Point measurePt, int frameNumber, TrackingManager* trackingManager)
{
	KalmanFilter* new_kf = this->init_kf(measurePt);
	this->kalmanfilters.push_back(new_kf);	
	this->active_kalmanfilters.push_back(-1);
	trackingManager->addFilterToTrack(measurePt, frameNumber);	
}

void FilterManager::remove_kf(int ID)
{
	vector<KalmanFilter*> newKalmanfilters;
	vector<int> newActiveKalmanFilters;
	for(int i = 0; i<kalmanfilters.size(); i++)
	{
		if(i == ID) {
			//TrackingManager::removeFromTracker(i);
			continue;
		}
		newActiveKalmanFilters.push_back(this->active_kalmanfilters[i]);
		newKalmanfilters.push_back(this->kalmanfilters[i]);
	}
	this->kalmanfilters = newKalmanfilters;
	this->active_kalmanfilters = newActiveKalmanFilters;
}

void FilterManager::updateKalmanFilters(vector<Rect> found, vector<int> matched) 
{
	Mat_<float> measurement(2,1); measurement.setTo(Scalar(0));
	for(int i = 0; i < kalmanfilters.size(); i++ )
 	{
		if(matched[i] == -1) {
			//No measurement, use prediction as best possible measurement
			kalmanfilters[i]->predict();
		} else {
			//get measurment
			Rect r = found[matched[i]];						
		
			// the HOG detector returns slightly larger rectangles than the real objects.
			// so we slightly shrink the rectangles to get a nicer output
        		r.x += cvRound(r.width*0.1);
        		r.width = cvRound(r.width*0.8);
        		r.y += cvRound(r.height*0.07);
        		r.height = cvRound(r.height*0.8);
        		//rectangle(frame2, r.tl(), r.br(), cv::Scalar(0,255,0), 2);
			//rectangle(frame2, r.tl(), r.br(), cv::Scalar(0,255,0), 2);
			
			measurement(0) = r.x+0.5*r.width;
			measurement(1) = r.y+0.5*r.height;
			kalmanfilters[i]->correct(measurement);
		}
	}
}
 
void FilterManager::updateKF(vector<Rect> found) 
{
	Mat_<float> measurement(2,1); measurement.setTo(Scalar(0));

	int NrKF = kalmanfilters.size();
	int NrMeas = found.size();

	int distThreshold = 35;
	vector<vector<int> > A;

	for(int i = 0; i<kalmanfilters.size(); i++) 
	{
		vector<int> distances;
		for(int j = 0; j<found.size(); j++) 
		{
			double dist = 0;
			Mat prediction = kalmanfilters[i]->predict();
			Point predictPt(prediction.at<float>(0),prediction.at<float>(1));
			dist = sqrt(pow(((found[j].x+0.5*found[j].width)-predictPt.x),2)+pow(((found[j].y+0.5*found[j].height)-predictPt.y),2));
			if(dist <= distThreshold) 
			{
				distances.push_back(dist);
			} 
			else 
			{
				distances.push_back(9999);
			}
		}
		A.push_back(distances);
	}
	//Check if measurement is missing or KF must be created
	vector<bool> kf;
	vector<bool> ms;
	kf.resize(kalmanfilters.size());
	ms.resize(found.size());
	fill(kf.begin(), kf.end(), false);
	fill(ms.begin(), ms.end(), false);
	for(int i = 0; i<A.size(); i++) 
	{
		for(int j = 0; j<A[i].size(); j++) 
		{
			if(A[i].at(j) <= distThreshold) 
			{
				kf[i] = true;
				ms[j] = true;
			}
		}
	}
	Hungarian hungarian2(A , NrKF, NrMeas, HUNGARIAN_MODE_MINIMIZE_COST) ;
	//hungarian2.print_cost();
	/*for(int i = 0; i<kf.size(); i++) {
		if(kf[i] == false) {
			 << "INGEN FILTER N�RE NOK" << endl;
			//Predict
			for(int j = 0; j<A[i].size(); j++) {
				A[i].at(j) = 0;
			}
		}
	}
	for(int i = 0; i<ms.size(); i++) {
		if(ms[i] == false) {
			//Wrong measurements.. remove
			 << "IGNORERER MEASUREMENT" << endl;
			for(int j = 0; j<A.size(); j++) {
				A[j].at(i) = 0;
			}
		}
	}*/
	//Solve global nearest neighbor
	Hungarian hungarian(A , NrKF, NrMeas, HUNGARIAN_MODE_MINIMIZE_COST) ;
	//hungarian.print_cost();
	/* solve the assignement problem */
	hungarian.solve();
	vector<vector<int> > assigned = hungarian.getAssignment();
	for(int i = 0; i<A.size(); i++) 
	{
		for(int j = 0; j<A[i].size(); j++) 
		{
			if (assigned[i].at(j) == 1 && kf[i] == true && ms[j] == true) 			    
			{
				//KF assigned -> Update and tick
				Rect r = found[j];						
				// the HOG detector returns slightly larger rectangles than the real objects.
				 // so we slightly shrink the rectangles to get a nicer output
				r.x += cvRound(r.width*0.1);
				r.width = cvRound(r.width*0.8);
				r.y += cvRound(r.height*0.07);
				r.height = cvRound(r.height*0.8);
				measurement(0) = r.x+0.5*r.width;
				measurement(1) = r.y+0.5*r.height;
				kalmanfilters[i]->correct(measurement);
				active_kalmanfilters[i] += 1;
			}
		}
	}

}

vector<int> FilterManager::matchKFToMeasurement(vector<Rect> found, int frameNumber, TrackingManager* trackingManager) 
{
	/* Global Nearest Neighbor Implementation 
	   If measurment is not found, predict!
	   If measurment is from no kalmanfilter -> create new filter */
	vector<int> matched;
	matched.resize(kalmanfilters.size());
	fill(matched.begin(), matched.end(), -1); 

	int NrKF = kalmanfilters.size();
	int NrMeas = found.size();

	int distThreshold = 35;

	vector<vector<int> > A;

	for(int i = 0; i<kalmanfilters.size(); i++) 
	{
		vector<int> distances;
		for(int j = 0; j<found.size(); j++) 
		{
			double dist = 0;
			Mat prediction = kalmanfilters[i]->predict();
			Point predictPt(prediction.at<float>(0),prediction.at<float>(1));
			dist = sqrt(pow(((found[j].x+0.5*found[j].width)-predictPt.x),2)+pow(((found[j].y+0.5*found[j].height)-predictPt.y),2));
			if(dist <= distThreshold) 
			{
				distances.push_back(dist);
			} 
			else 
			{
				distances.push_back(9999);
			}
		}
		A.push_back(distances);
	}
	//Check if measurement is missing or KF must be created
	vector<bool> kf;
	vector<bool> ms;
	kf.resize(kalmanfilters.size());
	ms.resize(found.size());
	fill(kf.begin(), kf.end(), false);
	fill(ms.begin(), ms.end(), false);
	for(int i = 0; i<A.size(); i++) 
	{
		for(int j = 0; j<A[i].size(); j++) 
		{
			if(A[i].at(j) <= distThreshold) 
			{
				kf[i] = true;
				ms[j] = true;
			}
		}
	}
	/*
	for(int i = 0; i<kf.size(); i++) {
		if(kf[i] == false) {
			//Predict (already done)
			//DONT SET TO 0 -> YIELDS WRONG ANSWER
			for(int j = 0; j<A[i].size(); j++) {
				A[i].at(j) = 0;
			} 
		}
	}*/
	for(int i = 0; i<ms.size(); i++) 
	{
		if(ms[i] == false) 
		{
			Point p((found[i].x+0.5*found[i].width),(found[i].y+0.5*found[i].height));
			add_kf(p, frameNumber, trackingManager);		
			matched.push_back(i);
			//DONT SET TO ZERO -> YIELDS WRONG ANSWER
			/*for(int j = 0; j<A.size(); j++) {
				A[j].at(i) = 0;
			}*/
		}
	}
	//Solve global nearest neighbor
	Hungarian hungarian(A , NrKF, NrMeas, HUNGARIAN_MODE_MINIMIZE_COST) ;

	/* solve the assignement problem */
	hungarian.solve();
	vector<vector<int> > assigned = hungarian.getAssignment();
	for(int i = 0; i<A.size(); i++) 
	{
		for(int j = 0; j<A[i].size(); j++) 
		{
			if (assigned[i].at(j) == 1 && kf[i] == true && ms[j] == true) 
			{
				matched[i] = j;
			}
		}
	}
	return matched;
}

void FilterManager::removeInactiveFilters(TrackingManager* trackingManager) 
{
	for(int i=0; i<kalmanfilters.size(); i++) 
	{
			//Check if it is observed within last 5 frames
			//and reset counter
			if(active_kalmanfilters[i] == -1 || active_kalmanfilters[i] == 5) 
			{
				remove_kf(i);
				trackingManager->removeFromTracker(i);
			}
			active_kalmanfilters[i] = 5;
	}
}

int FilterManager::getNumberOfTrackedObjects() 
{
	return this->kalmanfilters.size();
}

vector<KalmanFilter*>* FilterManager::getKalmanFilters() 
{
	return &kalmanfilters;
}

vector<int>* FilterManager::getActiveFilters() 
{
	return &active_kalmanfilters;
}

void FilterManager::printMatched(vector<int> matched) 
{
	for(int i = 0; i<matched.size(); i++) 
	{
		printf("%d ", matched[i]);
	}
	printf("\n");
}

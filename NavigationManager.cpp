#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <eigen3/Eigen/Dense>

#include "FilterManager.h"
#include "TrackingManager.h"
#include "NavigationManager.h"
#include "hungarian.h"

#include <cmath>

using namespace Eigen;
using namespace std;
using namespace cv;

const double PI = 4.0*atan(1.0);

NavigationManager::NavigationManager() {
	camOffset = 45;
	x_app = 0;
	y_app = 0;
	imgCntr = Point(0,0);
	f = 0.009;
	K << f, 0, imgCntr.x, 0, f, imgCntr.y, 0, 0, 1;
}

NavigationManager::NavigationManager(double offset, Point imgSize, Point fieldOfView, double focal_length) {
	camOffset = offset;
	x_app = fieldOfView.x/imgSize.x;
	y_app = fieldOfView.y/imgSize.y;
	imgCntr = Point(imgSize.x/2, imgSize.y/2);
	f = focal_length;
	K << f, 0, imgCntr.x, 0, f, imgCntr.y, 0, 0, 1;
}

Point3d NavigationManager::getRealWorldCoordinates(Point imgPoint, double height, double roll)
{
	double theta = 90-roll-camOffset;
	double theta_o = theta+(imgPoint.y-imgCntr.y)*y_app;
	double z_object = height/cos(((theta_o/180))*PI);
	double z = z_object*cos(((theta_o-theta)/180)*PI);

	double x = (imgPoint.x/f)*z;
 	double y = (imgPoint.y/f)*z;

	Point3d body_obj_loc(x,y,z);
	
	return body_obj_loc;
}

Point3d NavigationManager::getTargetCoordBodyFrame(Point imgPoint, double roll, double pitch, double heading, double z)
{
	Matrix3d R_r;
	Matrix3d R_p;
	Matrix3d R_y;
	R_r << 1, 0, 0, 0, cos((roll/180)*PI), -sin((roll/180)*PI), 0, sin((roll/180)*PI), cos((roll/180)*PI);
	R_p << cos((pitch/180)*PI), 0, sin((roll/180)*PI), 0, 1, 0, -sin((roll/180)*PI), 0, cos((roll/180)*PI);
	R_y << cos((heading/180)*PI), -sin((heading/180)*PI), 0, sin((heading/180)*PI), cos((heading/180)*PI), 0, 0, 0, 1;
	
	Matrix3d R = R_r*R_p*R_y;	
	Matrix3d H = this->K*R*this->K.transpose();

	Vector3d p(imgPoint.x, imgPoint.y, f);
	
	Vector3d new_p;
	new_p = H*p;

	Point3d loc((new_p[0]/f)*z, (new_p[1]/f)*z, z);

	return loc;
}

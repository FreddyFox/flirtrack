#ifndef _NavigationManager
#define _NavigationManager

using namespace Eigen;
using namespace std;
using namespace cv;

class NavigationManager {
	private:
		//The angle the camera is mounted on the airplane with
		double camOffset;
		double f;
		//The angle per pixel in x and y direction
		double x_app;
		double y_app;
		//Image coordinates of image center
		Point imgCntr;
		Matrix3d K;
	public:
		NavigationManager();
		NavigationManager(double, Point, Point, double);
		Point3d getRealWorldCoordinates(Point, double, double);
		Point3d getTargetCoordBodyFrame(Point, double, double, double, double);
	};

#endif

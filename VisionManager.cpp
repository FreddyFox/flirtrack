#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "FilterManager.h"
#include "TrackingManager.h"
#include "VisionManager.h"

using namespace std;
using namespace cv;

VisionManager::VisionManager() 
{
	//Set up HOG Descriptor
	HOGDescriptor hog;
   	hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());
	//Set up haar classifier
	CascadeClassifier haar;
	haar.load("haarcascade_fullbody.xml");

	setHOGClassifier(hog);
	setHAARClassifier(haar);
	videoStream_name = "Capture";
}

VisionManager::VisionManager(string filename, HOGDescriptor hog, CascadeClassifier haar) 
{
	setHOGClassifier(hog);
	setHAARClassifier(haar);
	videoStream_name = filename;
}

void VisionManager::setHOGClassifier(HOGDescriptor hog)
{
	this->hog = hog;
}

void VisionManager::setHAARClassifier(CascadeClassifier haar)
{
	this->haar = haar;
}

vector<Rect> VisionManager::searchImage(Mat img, bool detection_mode)
{
	vector<Rect> found;
	if(detection_mode) {
		found = this->detectPeople_HOG(img, this->hog);
	} else {
		found = this->detectPeople_HAAR(img, this->haar);
	}
	return found;
}

const char* VisionManager::getVideoStream() 
{
	return this->videoStream_name.c_str();
}

Ptr<FeatureDetector> VisionManager::createBlobDetector() 
{
	////TEST
	SimpleBlobDetector::Params params;
	params.minDistBetweenBlobs = 60.0f;
	params.minThreshold = 160;
   	params.maxThreshold = 230;
    	params.thresholdStep = 10;
	params.filterByInertia = false;
	params.filterByConvexity = false;
	params.filterByColor = false;
	params.filterByCircularity = false;
	params.filterByArea = true;
	params.minArea = 10.0f;
	params.maxArea = 10000.0f;
	params.filterByColor = false;
    	params.filterByCircularity = false;
    	// ... any other params you don't want default value

	// set up and create the detector using the parameters
	Ptr<FeatureDetector> blob_detector = new SimpleBlobDetector(params);
	blob_detector->create("SimpleBlob");
	return blob_detector;
}

vector<Rect> VisionManager::findROI_blobs(vector<KeyPoint> keypoints, Mat img) 
{
	vector<Rect> ROIs;
	drawKeypoints( img, keypoints, img, CV_RGB(0,255,0), DrawMatchesFlags::DEFAULT);
	for(int i = 0; i<keypoints.size(); i++) {
		cout << keypoints[i].size << endl;
	}
	//loop through keypoints, returning a rect for each point minus the inliers
	return ROIs;
}

bool VisionManager::segmentation_check(Mat roi)
{
	int histSize = 2;
	float range[] = {0, 256};
	const float* histRange = { range };
	int channel = 0;	
	const int* channels = &channel;
	bool uniform = true;
	bool accumulate = false;
	Mat hist;
	calcHist(&roi, 1, channels, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);
	printf("Ratio:%f\n", hist.at<float>(1)/(roi.rows*roi.cols));
	// > 0.15 limit found by testing
	
	if(hist.at<float>(1)/(roi.rows*roi.cols) > 0.15) 
	{
		return true;
	} else {
		//Not enough white in binary img
		return false;
	}
	return true;
}

Mat VisionManager::convertToGreyScale(Mat img)
{
	Mat grey;
	cvtColor(img, grey, CV_RGB2GRAY);
	return grey;
}

Mat VisionManager::binarizeImage(Mat img, int treshold) 
{
	Mat binarized = img;//255-img;
	//normalize(img, binarized, 20000, NORM_L1);
	//medianBlur(grey, grey, 5); //Blur gives better result (better edge-detection) Neg: More CPU power		
	//adaptiveThreshold(img, binarized, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 101, 5);
	threshold(binarized, binarized, treshold, 255, 0);
	//inRange(img, 40, 90, img);// is also a good function
	return binarized;
}

Mat VisionManager::isolateHumansBinarizedImg(Mat img) 
{
	Mat isolated;
	//Human-like box (rectangle)
	Mat element5(15,5,CV_8U,Scalar(1));
	//Mat element5(10,2,CV_8U,Scalar(1));
	//Disolve - Evolve to pick out rectangle like figures
	morphologyEx(img,isolated,MORPH_OPEN,element5); 
	/*vector<vector<Point> > contours;
	//grey = 255-grey; <- Invert is an idea that should be tested		
	findContours(isolated, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
	drawContours(isolated, contours, -1, Scalar::all(255), CV_FILLED);
	//grey = 255-grey;*/
	return isolated;
}

vector<Rect> VisionManager::detectPeople_HOG(Mat img, HOGDescriptor hog) 
{ //, Rect r_real) {
	//RUN PPL DETECT
	vector<Rect> found, found_filtered;
       	// run the detector with default parameters. to get a higher hit-rate
       	// (and more false alarms, respectively), decrease the hitThreshold and
       	// groupThreshold (set groupThreshold to 0 to turn off the grouping completely).
	// FOR THE PANDABOARD -> hog.detectMultiScale(frame2, found, 0, Size(4,4), Size(2,2), 1.2, 1);
	//hog.detectMultiScale(frame2, found, 0, Size(8,8), Size(2,2), 1.2, 1);

	if(img.rows < 128 || img.cols < 64) {
		//cout << "TOO BAD" << endl;
		return found;
	}

	hog.detectMultiScale(img, found, 0, Size(8,8), Size(2,2), 1.2, 1);
      	
	//detectMultiScale sometimes return a box inside a box, hence we filter these cases out
	size_t i, j;
    	for(i = 0; i < found.size(); i++ )
    	{
		Rect r = found[i];
       		for(j = 0; j < found.size(); j++ )
        	{
			if( j != i && (r & found[j]) == r)
			{
				break;
			}
		}
		Rect r2 = findProperRect(r, img);
		cout << r2.x << " " << r2.br() << " " << r2.y << " " << r2.tl() << endl;
		Mat roi = img(r2);
		
		if( j == found.size() && segmentation_check(roi))
		{
			//r.x += r_real.x;
			//r.y += r_real.y;
			found_filtered.push_back(r);
		}		
	}
	return found_filtered;
}

vector<Rect> VisionManager::detectPeople_HAAR(Mat img, CascadeClassifier haar) 
{
	//RUN PPL DETECT
	vector<Rect> found, found_filtered;
       	// run the detector with default parameters. to get a higher hit-rate
       	// (and more false alarms, respectively), decrease the hitThreshold and
       	// groupThreshold (set groupThreshold to 0 to turn off the grouping completely).
	// FOR THE PANDABOARD -> hog.detectMultiScale(frame2, found, 0, Size(4,4), Size(2,2), 1.2, 1);
	//hog.detectMultiScale(frame2, found, 0, Size(8,8), Size(2,2), 1.2, 1);
	//haar.detectMultiScale(img, found, 1.1, 1, CV_HAAR_SCALE_IMAGE, Size(50, 117), Size(90,210));
	Mat search = 255-img;
	//haar.detectMultiScale(search, found, 1.1, 0, 0, Size(16,32), Size(48,96));
	haar.detectMultiScale(search, found, 1.1, 1, 0, Size(42, 84), Size(84,168));
	//haar.detectMultiScale(search, found, 1.1, 0, 1.1, 3, 0, Size(16,32), Size(48,96));
	//haar.detectMultiScale(search, found, 1.1, 0, 0, Size(25,53), Size(30,60));
       	
	//detectMultiScale sometimes return a box inside a box, hence we filter these cases out
	size_t i, j;
    	for(i = 0; i < found.size(); i++ )
    	{
    		Rect r = found[i];
    		for(j = 0; j < found.size(); j++ )
        	{
			if( j != i && (r & found[j]) == r)
			{
				break;
			}
		}
		Rect r2 = findProperRect(r, img);
		Mat roi = img(r2);
		
		if( j == found.size() && segmentation_check(roi))
		{
			found_filtered.push_back(r);
		}		
	}
	//groupRectangles(found_filtered, 0, 0.5);
	return found_filtered;
}

vector<Rect> VisionManager::filterRectangles(vector<Rect> found) 
{
	vector<Rect> found_filtered;
	vector<Rect> found_filtered_temp;
	if(found.size() == 0) return found_filtered;
	found_filtered.push_back(found[0]);
	Rect temp = found[0];
	temp.x -= cvRound(temp.width*0.2);
    	temp.width = cvRound(temp.width*1.4);
    	temp.y -= cvRound(temp.height*0.2);
    	temp.height = cvRound(temp.height*1.4);
	found_filtered_temp.push_back(temp);
	size_t i, j;
    	for(i = 1; i < found.size(); i++ )
    	{
		Rect r = found[i];
		Rect temp = r;

       	for(j = 0; j < found_filtered_temp.size(); j++ )
		{
			if( j != i && (r & found_filtered_temp[j]) == r)
			{
				break;
			}
		}
		if( j == found_filtered_temp.size())
		{
			temp.x -= cvRound(temp.width*0.2);
			temp.width = cvRound(temp.width*1.4);
			temp.y -= cvRound(temp.height*0.2);
			temp.height = cvRound(temp.height*1.4);
			found_filtered.push_back(r);
			found_filtered_temp.push_back(temp);
		}		
	}
	return found_filtered;
}

Rect VisionManager::findProperRect(Rect r, Mat img)
{	
	Rect r2 = r;
	if(r.x < 0) r2.x = 0;
	if(r.y < 0) r2.y = 0;
	if(r2.x+r.width>=img.cols) r2.width = img.cols-r2.x-1;
	if(r2.y+r.height>=img.rows-1) r2.height = img.rows-r2.y-1;
	return r2;
}

void VisionManager::drawKalmanFilters(Mat img, vector<KalmanFilter*>* kalmanfilters, vector<int>* kalmanfilters_active)
{
	for(int i = 0; i<kalmanfilters->size(); i++)
	{
		if(kalmanfilters_active->at(i) >= 5) {
			//Predict the state
			Mat prediction = kalmanfilters->at(i)->statePost;
			Point predictPt(prediction.at<float>(0),prediction.at<float>(1));
		
			//Create rectangle		
			Rect e;
			e.x = predictPt.x-20; 
			e.y = predictPt.y-20;
			e.width = 40; 
			e.height = 40;
			rectangle(img, e.tl(), e.br(), cv::Scalar(0,0,255), 2);
		}
	}
}

void VisionManager::drawMeasurments(Mat img, vector<Rect> found) 
{
	for(int i = 0; i < found.size(); i++ )
	{
	//GET RECTANGLE X,Y
		Rect r = found[i];						
		
		// the HOG detector returns slightly larger rectangles than the real objects.
		// so we slightly shrink the rectangles to get a nicer output
        /*
		r.x += cvRound(r.width*0.1);
        r.width = cvRound(r.width*0.8);
        r.y += cvRound(r.height*0.07);
        r.height = cvRound(r.height*0.8);*/
        //rectangle(frame2, r.tl(), r.br(), cv::Scalar(0,255,0), 2);
		rectangle(img, r.tl(), r.br(), cv::Scalar(0,255,0), 2);
	}
}

void VisionManager::drawMeasurments(Mat img, vector<Rect> found, Point offset) 
{
	for(int i = 0; i < found.size(); i++ )
	{
	//GET RECTANGLE X,Y
		Rect r = found[i];						
        //rectangle(frame2, r.tl(), r.br(), cv::Scalar(0,255,0), 2);
		rectangle(img, r.tl(), r.br(), cv::Scalar(0,255,0), 2);
	}
}

void VisionManager::drawSearchRegion(Mat img, Rect r) 
{
    rectangle(img, r.tl(), r.br(), cv::Scalar(255,0,0), 2);
}


vector<Rect> VisionManager::findROI_contours(Mat segmented) 
{
	//COUNTOUR TESTING
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	// Find contours
	findContours( segmented, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

	// Approximate contours to polygons + get bounding rects and circles
	vector<Rect> boundRect( contours.size() );
	for(int i = 0; i < contours.size(); i++) {
		boundRect[i] = boundingRect( Mat(contours[i]) );
	}
	//FILTER
	//groupRectangles(boundRect, 0, 1000);
	vector<Rect> filtered;
	//Funksjon som merger alle boxer, selv de med bare NOE til felles

	/*size_t j;
	for(int i = 0; i < boundRect.size(); i++ )
    {
		if(boundRect[i].width > 70 || boundRect[i].height < 50) {
			cout << boundRect[i].width << " " << boundRect[i].height << endl;
			cout << "HELLO" << endl;
			break;
		}
		Rect r = boundRect[i];
        for(j = 0; j < boundRect.size(); j++ )
			{
			if( j != i && (r & boundRect[j]) == r)
				{
					break;
				}
		}
		if( j == boundRect.size())
		{
			filtered.push_back(r);
		}		
	}
	groupRectangles(filtered, 1, 3);*/
	filtered = boundRect;
	for( int i = 0; i < filtered.size(); i++ )
	{ 
		filtered[i].x = filtered[i].x - filtered[i].width;
		filtered[i].y = filtered[i].y - filtered[i].height;
		filtered[i].width = filtered[i].width*4;
		filtered[i].height = filtered[i].height*3;
		if(filtered[i].x < 0) filtered[i].x = 0;
		if(filtered[i].x+filtered[i].width > segmented.cols) filtered[i].width = segmented.cols-filtered[i].x-1;
		if(filtered[i].y < 0) filtered[i].y = 0;
		if(filtered[i].y+filtered[i].height > segmented.rows) filtered[i].height = segmented.rows-filtered[i].y-1;
    	}
	
	return filtered;

}

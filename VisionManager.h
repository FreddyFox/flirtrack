#ifndef _ComputerVision
#define _ComputerVision

using namespace std;
using namespace cv;

class VisionManager {
	private:
		HOGDescriptor hog;
		CascadeClassifier haar;
		string videoStream_name;
	public:
		VisionManager();
		VisionManager(string, HOGDescriptor hog, CascadeClassifier);
		void setHOGClassifier(HOGDescriptor);
		void setHAARClassifier(CascadeClassifier);
		vector<Rect> searchImage(Mat, bool);
		const char* getVideoStream();
		Ptr<FeatureDetector> createBlobDetector();
		bool segmentation_check(Mat);
		Mat convertToGreyScale(Mat);
		Mat binarizeImage(Mat, int);
		Mat isolateHumansBinarizedImg(Mat);
		vector<Rect> detectPeople_HOG(Mat, HOGDescriptor);
		vector<Rect> detectPeople_HAAR(Mat, CascadeClassifier);
		vector<Rect> filterRectangles(vector<Rect>);
		Rect findProperRect(Rect, Mat);
		void drawKalmanFilters(Mat, vector<KalmanFilter*>*, vector<int>*);
		void drawMeasurments(Mat, vector<Rect>);
		void drawMeasurments(Mat, vector<Rect>, Point);
		void drawSearchRegion(Mat, Rect);
		vector<Rect> findROI_contours(Mat);
		vector<Rect> findROI_blobs(vector<KeyPoint>, Mat);
};

#endif

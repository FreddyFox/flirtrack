#include <stdio.h>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "FilterManager.h"
#include "TrackingManager.h"
#include "VisionManager.h"

using namespace std;
using namespace cv;

TrackingManager::TrackingManager(string n) 
{
	/* Initializes the dataLogg vector */
	vector<vector<Point*>* >* dataLogg = new vector<vector<Point*>* >;
	name = n;
}

void TrackingManager::writeTrackToFile(int ID) 
{
	/* Writes a position trajectory to file when target is lost
	   Input: ID = id of target that is lost
	   Output: None 					*/
	ofstream outputFile;
	stringstream ss;
	vector<Point*>* track = dataLogg->at(ID);
	ss << "tracks/" << track->at(0)->x << "-" << track->at(0)->x+track->size()-1 << ".txt";
	outputFile.open(ss.str().c_str());
	if(outputFile) {
		for(int j = 1; j < track->size()-1; j++) {
			outputFile << track->at(j)->x << " ";
		}
		outputFile << endl << endl;
		for(int j = 1; j < track->size()-1; j++) {
			outputFile << track->at(j)->y << " ";
		}
		outputFile <<  endl << endl;
		for(int j = 1; j < track->size()-1; j++) {
			outputFile << track->at(0)->x+j-1 << " ";
		}
	}
	outputFile.close();
}

void TrackingManager::addFilterToTrack(Point start, int frameNumber) 
{
	/* Adds a new target to the tracking manager.
	   Input: Start = Initial position measurement
		  frameNumber = processed frame #
	   Output: None 				*/ 
	vector<Point*>* track = new vector<Point*>;
	Point* frame = new Point;
	frame->x = frameNumber;
	frame->y = 0;
	track->push_back(frame);
	track->push_back(new Point(start.x,start.y));
	dataLogg->push_back(track);
}

void TrackingManager::keepTrack(vector<KalmanFilter*>* kalmanfilters) 
{
	/* Loggs new position points of the tracked targets to the
	   dataLogg.
	   Input: kalmanfilters = a list of current targets and
		  their Kalman filters
	   Output: None						*/
	for(int i = 0; i<kalmanfilters->size(); i++) {
		Point* p = new Point;
		p->x = kalmanfilters->at(i)->statePost.at<float>(0);
		p->y = kalmanfilters->at(i)->statePost.at<float>(1);
		dataLogg->at(i)->push_back(p);
	}
}

void TrackingManager::removeFromTracker(int ID) 
{
	/* Removes a target from the dataLogg, and calls the
	   storage functions writeToTrack(ID)
	   Input: ID = id of target that is to be remove
	   Output: None						*/
	vector<vector<Point*>* >* newTracks = new vector<vector<Point*>* >;
	writeTrackToFile(ID);
	for(int i=0; i<dataLogg->size(); i++) {
		if (i == ID) {
			continue;
		}
		newTracks->push_back(dataLogg->at(i));
	}
	dataLogg = newTracks;
}


void TrackingManager::moveToKeepTrack(Point imgSize, vector<KalmanFilter> kalmanfilters)
{
	/* Navigation algorithm that draws a blue circle in the direction
	   that the UAV should move. Does not communicate with the autopilot.
	   Input: imgSize = the number of cols and rows in the image
	   	  kalmanfilters = a list of the targets being tracked, and
				  their respective Kalman filters
	   Output: None							 */
	Mat predict;
	
	for(int i = 0; i < kalmanfilters.size(); i++)
	{
		predict = kalmanfilters[i].predict();
		Point predictPt(predict.at<float>(0), predict.at<float>(1));
		if(predictPt.x > 0.9*imgSize.x)
		{
			if(predictPt.y > 0.9*imgSize.y)
			{
				cout << "Move right + down" << endl;
			} 
			else if (predictPt.y < 0.1*imgSize.y) 
			{
				cout << "Move right + up" << endl;
			} 
			else 
			{
				cout << "Move right" << endl;
			}
			break;
		} else if (predictPt.x < 0.1*imgSize.x) {
			if(predictPt.y > 0.9*imgSize.y)
			{
				cout << "Move left + down" << endl;
			} 
			else if (predictPt.y < 0.1*imgSize.y) 
			{
				cout << "Move left + up" << endl;
			} 
			else 
			{
				cout << "Move left" << endl;
			}
			break;
			//UP AND DOWN ALSO
		}	
	}
}

vector<bool> TrackingManager::objectTrackingFailed(vector<KalmanFilter> kalmanfilters, vector<Rect> measurement)
{
	/* Keeps track of if the objects belonging to the kalman filters 
           have been detected in the last 5 seconds. If not, tracking has 
           failed / target is lost */
	vector<bool> trackingFailed;
	return trackingFailed;
}

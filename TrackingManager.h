#ifndef _TrackingManager
#define _TrackingManager

using namespace std;
using namespace cv;

class TrackingManager {
	private:
		string name; 
		vector<vector<Point*>* >* dataLogg;
		void writeTrackToFile(int ID);
	public:
		TrackingManager(string n);
		void keepTrack(vector<KalmanFilter*>*);
		void removeFromTracker(int);
		void addFilterToTrack(Point, int);
		void moveToKeepTrack(Point, vector<KalmanFilter>);
		vector<bool> objectTrackingFailed(vector<KalmanFilter>, vector<Rect>);
	};

#endif

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/features2d/features2d.hpp>

#include "FilterManager.h"
#include "TrackingManager.h"
#include "VisionManager.h"
//#include "NavigationManager.h"

#include <DUNE/DUNE.hpp>

using namespace std;
using namespace cv;

int main( int argc, char** argv )
{
  	int key = 0;
	int fps = 0;
			
	TrackingManager* trackingManager = new TrackingManager("image");	
	VisionManager* visionManager = new VisionManager();
	FilterManager* filterManager = new FilterManager();
  	//NavigationManager* navigationManager = new NavigationManager();

	VideoCapture videoStream;
	bool playback = false;
	cout << "Choose Option: " << endl << "1. Stream from CAM" << endl << "2. Stream from file" << endl;
	while(!(key == 1 || key == 2)){
		cin >> key;
	}
	if(key == 1) {
		videoStream.open(0);
		fps = 30;
	} else {
		videoStream.open(visionManager->getVideoStream());
		fps = ( int )videoStream.get(CV_CAP_PROP_FPS);
		playback = true;
	}

	if(!videoStream.isOpened())
	{
		cout << "Could not open video source" << endl;
		return -1;
	}	

	key = 0;
	
	//SET UP BLOB DETECTOR
	//Ptr<FeatureDetector> blob_detector = visionManager->createBlobDetector();

	bool detection_mode;

	cout << "Choose Option: " << endl << "1. HoG Descriptor" << endl << "2. Cascaded Haar Features" << endl;
	while(!(key == 1 || key == 2)){
		cin >> key;
	}
	if(key == 1) {
		detection_mode = true;
	} else {
		detection_mode = false;
	}


	string cam_window_name = "Tracking";
	string segmentation_window_name = "Segmentation";
	
	Mat frame(Size(1,1),CV_8UC1);
	Mat grey;

	int tick = 5;
	int frameNumber = 1;
	while( key != 'q' ) {
 		videoStream >> frame;
		if(frame.empty() )
		{ 
			cout << "Could not read image" << endl; 
			if(playback) break; else continue;
		}
		//Mat out = frame;
		//vector<KeyPoint> keypoints;
		//blob_detector->detect(out, keypoints);
		//findROI(keypoints, frame);
		//extract the x y coordinates of the keypoints: 
		//drawKeypoints( frame, keypoints, frame, CV_RGB(0,255,0), DrawMatchesFlags::DEFAULT);
		
		//resize(frame, frame, Size(352, 240));
		//resize(frame, frame, Size(352, 238));

		//START TIMER
		double t = (double)getTickCount();		
		
		//Convert to greyscale
		grey = visionManager->convertToGreyScale(frame);
		//cout << "NORM IS: " << norm(grey, NORM_L1) << endl;
		int normfactor = 70;
		normfactor = normfactor*frame.rows*frame.cols;
		normalize(grey, grey, normfactor, 0, NORM_L1);
		//cout << "NORM IS: " << norm(grey, NORM_L1) << endl; 
		//frame.copyTo(grey);
		//Binarize greyscale img
		grey = visionManager->binarizeImage(grey, 150);
		
		//Dillitate and erode
		grey = visionManager->isolateHumansBinarizedImg(grey);

		if(tick == 5) {
			tick = 0;
			filterManager->removeInactiveFilters(trackingManager);
			
			//EDGE INSTEAD OF SEGMENT?
			//blur(grey, grey, Size(3,3) );
			//Canny(grey, grey, 100, 200, 3, false);
			

			//FIND PROMISING REGIONS
			vector<KeyPoint> keypoints;
			//blob_detector->detect(frame, keypoints);
		
			//vector<Rect> ROIs = findROI_blobs(keypoints, frame);
		
			//vector<Rect> ROIs;// = findROI_contours(grey);
			/*for(int i = 0; i < ROIs.size(); i++) {
				rectangle(grey, ROIs[i].tl(), ROIs[i].br(), Scalar(255,255,255), 2, 8, 0);
			}
			vector<Rect> found;
			for(int i = 0; i < ROIs.size(); i++) {
				vector<Rect> current_found;
			
				if(detection_mode) {
					current_found = detectPeople_HOG(frame(ROIs[i]), hog, ROIs[i]);
				} else {
					current_found = detectPeople_HAAR(frame(ROIs[i]), haar);
				}
				found.insert(found.end(), current_found.begin(), current_found.end());
       		
			}*/
		
			vector<Rect> found;
			found = visionManager->searchImage(frame, detection_mode);
			//Match Kalman to measurements L2-wise
			vector<int> matched = filterManager->matchKFToMeasurement(found, frameNumber, trackingManager);
			filterManager->updateKalmanFilters(found, matched);

			cout << "5. FRAME: FANT " << found.size() << " MEASUREMENTS" << endl;

			//drawMeasurments(frame, boundRect);
			//drawMeasurments(frame, found);
			
		} else {
			tick += 1;
			vector<Point> already_taken;
			vector<Rect> found;
			for(int i = 0; i<filterManager->getNumberOfTrackedObjects(); i++) {
				//Size of Search Region
				int width = 80; int height = 150;

				vector<Rect> found_for_kf;
				int xlow; int xhigh; int ylow; int yhigh;
				Mat prediction = filterManager->getKalmanFilters()->at(i)->predict();
		        Point predictPt(prediction.at<float>(0),prediction.at<float>(1));
				xlow = predictPt.x-110; xhigh = predictPt.x+100;
				ylow = predictPt.y-200; yhigh = predictPt.y+150;
				/*xlow = predictPt.x-50; xhigh = predictPt.x+30;
				ylow = predictPt.y-100; yhigh = predictPt.y+75;*/
				if(predictPt.y-200 < 0) ylow = 0;
				if(predictPt.x-110 < 0) xlow = 0;
				if(predictPt.y+150 >= frame.rows) yhigh = frame.rows-1;
				if(predictPt.x+100 >= frame.cols) xhigh = frame.cols-1;
				
				found_for_kf = visionManager->searchImage(frame(Range(ylow, yhigh),Range(xlow, xhigh)), detection_mode);
				Point offset(xlow, ylow);
				//Point p;
				//if(found.size()) p = updateKalmanFilters(found, i, kalmanfilters, kalmanfilters_active, offset, already_taken);
				//if(p.x != 0 && p.y != 0) { already_taken.push_back(p); }
				Rect r(xlow,ylow,xhigh,yhigh);
				//drawMeasurments(frame, found_for_kf, offset);
				//drawSearchRegion(frame, r);// NB: DO NOT DRAW HERE, BECAUSE FRAME2 IS STILL BEING USED FOR DETECTION
				for(int i = 0; i<found_for_kf.size(); i++) {
					found_for_kf[i].x += offset.x;
					found_for_kf[i].y += offset.y;
				}
				found.insert(found.end(), found_for_kf.begin(), found_for_kf.end());
			}
			found = visionManager->filterRectangles(found);
			filterManager->updateKF(found);
			//drawMeasurments(frame, found);
		}

		visionManager->drawKalmanFilters(frame, filterManager->getKalmanFilters(), filterManager->getActiveFilters());
		trackingManager->keepTrack(filterManager->getKalmanFilters());
		
		imshow(cam_window_name, frame);
        	imshow(segmentation_window_name, grey);
		
		t = (double)getTickCount() - t;
		printf("detection time = %gms\n", t*1000./cv::getTickFrequency());

		//Tick a frame
		frameNumber += 1;
		frameNumber = frameNumber % 100000;

		//This is where control input should be sent
		//moveToKeepTrack(p, kalmanfilters);

		//Check if the program should quit
		key = waitKey( 1000 / fps );
		//key = cvWaitKey(0); //DEBUGGING
 	}
	return 0;
}

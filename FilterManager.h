#ifndef _KalmanFilter
#define _KalmanFilter

#include "TrackingManager.h"

using namespace std;
using namespace cv;

class FilterManager {
	private:
		vector<KalmanFilter*> kalmanfilters;
		vector<int> active_kalmanfilters;
		KalmanFilter* init_kf(Point);
		void add_kf(Point, int, TrackingManager*);
		void remove_kf(int);
	public:
		FilterManager();
		int getNumberOfTrackedObjects();
		vector<KalmanFilter*>* getKalmanFilters();
		vector<int>* getActiveFilters();
		void updateKF(vector<Rect>);
		void updateKalmanFilters(vector<Rect>, vector<int>);
		//void removeInactive(vector<int>, vector<KalmanFilter>&, vector<int>&);
		void removeInactiveFilters(TrackingManager*);
		vector<int> matchKFToMeasurement(vector<Rect>, int frameNumber, TrackingManager*);
		void printMatched(vector<int>);
};

#endif
